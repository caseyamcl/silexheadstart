SilexHeadStart is a OOP Silex-based framework for writing aresome PHP apps

Roadmap
-------
* Add and use a SilexCliAwareApp class that adds functionality for middlewares to optionally be included in Console runtime cycle
* Possibly separate components into separate packages  ($app->before(callback(), EARLY_EVENT, $app::INCLUDE_CLI))  '--INCLUDE_CLI = true'
* Move Twig fanciness into a Twig Extension Class
* Consider not using Twig site_url and base_url if possible, in favor of "url()"
* Change controllers to not be aware of the DiC container.  Don't pass libraries into a constructor, though; use a separate "register" or something to pass dependencies
* Consider moving many of the functions in the Controller base class into a separate ControllerUtilities class (or classes)
  init() function for that.  Make it an interface requirement, so we can add option parameters
* Change ModelMappers so that $om variable is passed at runtime, and the objects are initialized by a provider.  This way, 
  we can reserve the controller for dependencies.  Create an interface for Mappers and an interface for Doctrine-aware mappers
* Consider just making a dependency to a more mature Doctrine ODM provider
* Consider instead of controllers announcing the routes they map to, having a central registry of routes and their associated controller 
  actions
* Add logic that we used in the AndICanStudy app for Doctrine Form extensions to the dependencies for Mongo ODM in SilexHeadStart, including the
  Manager class we had to use
* Consider using Symfony built-in sessionBag for notices if it seems worthwhile