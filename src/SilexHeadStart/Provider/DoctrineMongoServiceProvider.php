<?php

namespace SilexHeadStart\Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Mongo;
use Doctrine\Common\ClassLoader as MongoClassLoader,
    Doctrine\Common\Annotations\AnnotationReader,
    Doctrine\ODM\MongoDB\DocumentManager,
    Doctrine\MongoDB\Connection as MongoConnection,
    Doctrine\ODM\MongoDB\Configuration as MongoConfiguration,
    Doctrine\ODM\MongoDB\Mapping\Driver\AnnotationDriver;

use Doctrine\Bundle\MongoDBBundle\Form\DoctrineMongoDBExtension;
use SilexHeadStart\Helper\DoctrineManagerRegistry;

/**
 * Doctrine Mongo Service Provider
 * Uses Doctrine Mongo ODM with Annotation Driver
 */
class DoctrineMongoServiceProvider implements ServiceProviderInterface
{
    public function register(Application $app)
    {   

        $app['mongo'] = $app->share(function($app) {

            $params = (isset($app['mongo.params']))
                ? $app['mongo.params']
                : array();

            $app['mongo.conn'] = $app->share(function($app) use ($params) {
                $conn = (isset($params['connstring']) && $params['connstring'])
                    ? new MongoConnection(new Mongo($params['connstring']))
                    : new MongoConnection();
            });

            AnnotationDriver::registerAnnotationClasses();

            //Params and their defaults
            $proxyDir = isset($params['proxy_dir'])
                ? $params['proxy_dir'] 
                : sys_get_temp_dir();

            $hydratorDir = isset($params['hydrator_dir'])
                ? $params['hydrator_dir'] 
                : sys_get_temp_dir();

            $dbName = isset($params['dbname'])
                ? $params['dbname'] 
                : $app['slug'] ?: 'silexapp';

            $docPath = isset($app['mongo.documents_path'])
                ? $app['mongo.documents_path']
                : __DIR__ . '/../Model';

            //Config
            $config = new MongoConfiguration();
            $config->setProxyDir($proxyDir);
            $config->setProxyNamespace('Proxies');
            $config->setHydratorDir($hydratorDir);
            $config->setHydratorNamespace('Hydrators');
            $config->setMetadataDriverImpl(AnnotationDriver::create($docPath));
            $config->setDefaultDB($dbName);

            //Document namesapce
            if (isset($app['mongo.documents_namespace'])) {
                $config->addDocumentNamespace($app['mongo.documents_namespace'], 'Tester');
            }

            //Set it up
            $dm = DocumentManager::create($app['mongo.conn'], $config);

            //Enforce indexes
            $dm->getSchemaManager()->ensureIndexes();

            //Add Doctrine Mongo Form Extensions, if the form class is loaded and we have included the Doctrine ODM Symfony Bridge
            if (isset($app['form.factory']) && class_exists('\Doctrine\Bundle\MongoDBBundle\Form\DoctrineMongoDBExtension')) {

                //$app['form.extensions'] - For the Mongo ODM Document Type
                $app['form.extensions'] = $app->share($app->extend('form.extensions', function($extensions) use ($app) {

                    //Add the Manager Registry extension for Mongo ODM Types
                    $managerRegistry = new DoctrineManagerRegistry(null, array(), array('mongo'), null, null, '\Doctrine\ODM\MongoDB\Proxy\Proxy');
                    $managerRegistry->setContainer($app);
                    $extensions[] = new DoctrineMongoDBExtension($managerRegistry);
                    return $extensions;

                }));
            }

            //Return the documentManager
            return $dm;
        });
    }

    // --------------------------------------------------------------

    public function boot(Application $app)
    {
        //nuthin
    }
}

/* EOF: DoctrineMongoServiceProvider.php */