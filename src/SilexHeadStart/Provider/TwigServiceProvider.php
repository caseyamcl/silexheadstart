<?php

namespace SilexHeadStart\Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Silex\Provider\TwigServiceProvider as SilexTwigServiceProvider;
use SilexHeadStart\Service\Notices;

use Symfony\Component\HttpFoundation\Request;
use Twig_Environment, Twig_SimpleFunction, Twig_Extension_Debug;
use Kint;

/**
 * Extend base Twig Provider by adding a few useful globals and support for debugging 
 */
class TwigServiceProvider extends SilexTwigServiceProvider implements ServiceProviderInterface
{
    /**
     * @param Silex\Application
     */
    private $app;

    // --------------------------------------------------------------

    /**
     * {@inherit}
     */
    public function boot(Application $app)
    {
        $this->app = $app;
        $app->before(array($this, 'setupTwig'), Application::EARLY_EVENT);

        return parent::boot($app);
    }

    // --------------------------------------------------------------

    /**
     * Setup Twig
     *
     * @param Symfony\HttpFoundation\Request
     */
    public function setupTwig(Request $request)
    {
        //Refer to the App
        $app  =& $this->app;
        $twig =& $app['twig'];

        $baseUrl    = $request->getSchemeAndHttpHost() . $request->getBasePath();
        $appUrl     = $request->getSchemeAndHttpHost() . $request->getBaseUrl();
        $currentUrl = $appUrl . $request->getPathInfo();

        //Register URLs as globals
        $twig->addGlobal('base_url',    $baseUrl);
        $twig->addGlobal('site_url',    $appUrl);
        $twig->addGlobal('current_url', $currentUrl); 

        //debug mode?
        if ($app['debug'] == true) {
            $twig->addExtension(new Twig_Extension_Debug());
            $twig->addGlobal('debug_mode', true);
            
            if (class_exists('Kint')) {
                $twig->addFunction(new Twig_SimpleFunction('kint', function($data) {
                    return Kint::dump($data);
                }));
            }
        }
        else {
            $twig->addGlobal('debug_mode', false);
        }

        //notices?
        if (isset($app['notices'])) {
            $twig->addFunction(new Twig_SimpleFunction(
                'notices', 
                array($app['notices'], 'flush')
            ));            
        }
        elseif (isset($app['session'])) {
            $twig->addFunction(new Twig_SimpleFunction(
                'notices',
                array(new Notices($app['session']), 'flush')
            ));
        }

        $this->extendTwig($app, $request, $twig);
    }

    // --------------------------------------------------------------

    /**
     * Override this in child classes to add further customization to Twig
     *
     * @param Silex\Application $app
     * @param Twig_Environment  $twig
     * @return Twig_Environment
     */
    protected function extendTwig(Application $app, Request $request, Twig_Environment $twig)
    {
        return $twig;
    }
}

/* EOF: TwigServiceProvider.php */