<?php

namespace SilexHeadStart\Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;
use SilexHeadStart\Helper\MapperCollection;

/**
 * Extends base Monolog Service Provider by also adding Chrome and Firefox debugging support
 */
class MapperCollectionProvider implements ServiceProviderInterface
{
    // --------------------------------------------------------------

    public function register(Application $app)
    {
        if ( ! isset($app['model.mappers.classnames'])) {
            $app['model.mappers.classnames'] = array();
        }

        $app['model.mappers'] = $app->share(function() use ($app) {

            $mapperObj = new MapperCollection();

            foreach($app['model.mappers.classnames'] as $clsName) {
                $mapperObj->addMapper(new $clsName($app['mongo']));
            }
            
            return $mapperObj;
        });
    }

    // --------------------------------------------------------------

    public function boot(Application $app)
    {
        //pass
    }    
}

/* EOF: MapperCollectionProvider.php */