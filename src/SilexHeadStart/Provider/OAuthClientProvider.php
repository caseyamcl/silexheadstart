<?php

namespace SilexHeadStart\Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;

use SilexHeadStart\Helper\AuthPluginCollection;

use OAuth\ServiceFactory;
use OAuth\Common\Http\Uri\UriFactory;
use OAuth\Common\Storage\SymfonySession;

use RuntimeException, Exception;

/**
 * Oauth Client Provider
 */
class OAuthClientProvider implements ServiceProviderInterface
{
    public function __construct()
    {
        if ( ! class_exists('OAuth\ServiceFactory')) {
            throw new Exception("The SilexHeadStart OAuth Client Provider requires the Composer package 'lusitanian/oauth'");
        }
    }

    public function register(Application $app)
    {
        //Service Factory
        $app['auth.plugins']       = new AuthPluginCollection();
        $app['oauth.svc_factory']  = new ServiceFactory();
        $app['oauth.storage']      = new SymfonySession($app['session']); 
        $app['oauth.autoregister'] = true;
        $app['oauth.params']       = array();
        $app['oauth.namespace']    = null;  
    }

    // --------------------------------------------------------------

    public function boot(Application $app)
    {
        if ( ! $app['oauth.namespace']) {
            throw new RuntimeException("oauth.namespace parameter is required");
        }

        $params = $app['oauth.params'];

        foreach($params as $name => $info) {

            $classname = rtrim($app['oauth.namespace'], "\\") . "\\" . ucfirst($name);

            if (class_exists($classname) && is_subclass_of($classname, '\SilexHeadStart\Authentication\OAuthProvider')) {

                $item = new $classname(
                    $app['oauth.svc_factory'],
                    $app['oauth.storage'],
                    $info['key'],
                    $info['secret'],
                    $app['oauth.autoregister']
                );

                $app['auth.plugins']->addPlugin($item);
            }
        }
    }
}

/* EOF: OAuthClientProvider.php */