<?php

namespace SilexHeadStart\Provider;

use Silex\Application;
use Silex\Provider\SwiftmailerServiceProvider as BaseSwiftmailerServiceProvider;
use Swift_Transport_SendmailTransport, Swift_Transport_MailTransport;
use Swift_Transport_SimpleMailInvoker, Swift_Message;

/**
 * Swift Mailer Service Provider with configurable tranpsort
 */
class SwiftmailerServiceProvider extends BaseSwiftmailerServiceProvider
{
    public function register(Application $app)
    {
        parent::register($app);

        //Default transport name
        $app['swiftmailer.transportname'] = 'smtp';
        
        //Default 'from' address
        $app['swiftmailer.defaultfrom'] = array('someapp@localhost' => 'Some App');

        //Override transport setup by parent if not 'smtp'
        $app['swiftmailer.transport'] = $app->share(function($app) {

            switch($app['swiftmailer.transportname']) {

                case 'sendmail':

                    $transport = new Swift_Transport_SendmailTransport($app['swiftmailer.transport.buffer'], $app['swiftmailer.transport.eventdispatcher']);

                    $options = $app['swiftmailer.options'] = array_replace(array(
                        'command' => '/usr/sbin/sendmail -bs'
                    ), $app['swiftmailer.options']);

                    $transport->setCommand($options['command']);

                    return $transport;
                case 'phpmail':
                    return new Swift_Transport_MailTransport(new Swift_Transport_SimpleMailInvoker(), $app['swiftmailer.transport.eventdispatcher']);
                case 'smtp':
                default:

                    //Copied from parent (I know; kludgy, but that's how it is coded)
                    $transport = new \Swift_Transport_EsmtpTransport(
                        $app['swiftmailer.transport.buffer'],
                        array($app['swiftmailer.transport.authhandler']),
                        $app['swiftmailer.transport.eventdispatcher']
                    );

                    $options = $app['swiftmailer.options'] = array_replace(array(
                        'host'       => 'localhost',
                        'port'       => 25,
                        'username'   => '',
                        'password'   => '',
                        'encryption' => null,
                        'auth_mode'  => null,
                    ), $app['swiftmailer.options']);

                    $transport->setHost($options['host']);
                    $transport->setPort($options['port']);
                    $transport->setEncryption($options['encryption']);
                    $transport->setUsername($options['username']);
                    $transport->setPassword($options['password']);
                    $transport->setAuthMode($options['auth_mode']);

                    return $transport;

                break;
            }
        });

        //Convenience method
        $app['mail.a.msg'] = $app->protect(function($to, $subj, $msg, $from = null) use ($app) {

            $message = Swift_Message::newInstance($subj)
                ->setFrom($from ?: $app['swiftmailer.defaultfrom'])
                ->setTo($to)
                ->setBody($msg);

            return $app['mailer']->send($message);
        });
    }
}

/* EOF: SwiftmailerServiceProvider.php */