<?php

namespace SilexHeadStart\Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use SilexHeadStart\Helper\AppContextInfo;

/**
 * Create application context info provider
 */
class AppContextInfoProvider implements ServiceProviderInterface
{
    /**
     * @param Silex\Application
     */
    private $app;

    // --------------------------------------------------------------

    public function register(Application $app)
    {
        $app['app.context'] = $app->share(function($app) {

            //Setup object
            $obj = (isset($app['app.context.obj']))
                ? $app['app.context.obj']
                : new AppContextInfo();
                

            //If CLI, init immediately (since 'boot' won't be run)
            if ($obj->getContext() == $obj::CLI) {
                $obj->init($app);
            }

            return $obj;
        });
    }

    // --------------------------------------------------------------

    public function boot(Application $app)
    {
        $app->before(function() use ($app) {
            $app['app.context']->init($app);
        }, Application::EARLY_EVENT);        
    }
}

/* EOF: AppContextInfoProvider.php */