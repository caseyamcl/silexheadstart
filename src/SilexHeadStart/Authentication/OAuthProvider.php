<?php

namespace SilexHeadStart\Authentication;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

use OAuth\Common\Storage\TokenStorageInterface;
use OAuth\Common\Consumer\Credentials;
use OAuth\ServiceFactory;

/**
 * Generic OAuth Service Provider for Authentication
 */
abstract class OAuthProvider implements ServiceProviderInterface
{
    /**
     * @var OAuth\ServiceFactory
     */
    private $svcFactory;

    /**
     * @var OAuth\Common\Storage\TokenStorageInterface
     */
    private $storage;

    /**
     * @var string
     */ 
    private $key;

    /**
     * @var string
     */
    private $secret;

    /**
     * @var boolean
     */
    private $canCreateNew;

    // --------------------------------------------------------------

    /**
     * Constructor
     */
    public function __construct(ServiceFactory $svcFactory, TokenStorageInterface $storage, $key, $secret, $canCreateNew = false)
    {
        $this->svcFactory   = $svcFactory;
        $this->storage      = $storage;
        $this->canCreateNew = (boolean) $canCreateNew;

        $this->key    = $key;
        $this->secret = $secret;
    }

    // --------------------------------------------------------------

    /**
     * Get service parameters (scope, etc)
     * 
     * @return array
     */
    abstract protected function getServiceParams();

    /**
     * Get the URL for which to find user information after registering
     *
     * @return string
     */
    abstract protected function getInfoUrl();

    /**
     * Map information provided by the service's info URL to an array
     *
     * Include the following:
     * [
     *   'email'     => '...' //required
     *   'firstName' => '...' //optional
     * ]
     *
     * The array can also contain other elements, which will be stored with
     * the object and associated with this service
     *
     * @param  string  $rawReturnedData
     * @return array
     */
    abstract protected function mapResponseToUserInfo($rawReturnedData);

    /**
     * An identifier to identify the service -- Maps to Oauth Factory identifiers
     *
     * @return string
     */
    abstract public function getSlug();

    // --------------------------------------------------------------

    /**
     * Can this plugin create a new user on successful authentication?
     *
     * @return boolean
     */
    public function canCreateNewUser()
    {
        return $this->canCreateNew;
    }

    // --------------------------------------------------------------

    /**
     * Get some sample HTML that comprises the link to this service login
     * 
     * @param  string $url  The complete URL to the service provder
     * @return string       HTML
     */
    public function getLinkHtml($url)
    {
        $name = ucfirst($this->getSlug());
        return "<a href='$url' title='Login with $name'>$name</a>";
    }

    // --------------------------------------------------------------

    /**
     * Perform the login action
     *
     * Return an array of identifiers for the user if login succeeds,
     * false if login fails, or a RedirectResponse if the user should be
     * redirected to another URL as an intermediary step
     *
     * @param Symfony\Component\HttpFoundation\Request $request
     * @return array|boolean|Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function loginAction(Request $request)
    {
        //Setup a crednetials object
        $currUrl = $request->getSchemeAndHttpHost() . $request->getBaseUrl() . $request->getPathInfo();
        $credentials = new Credentials($this->key, $this->secret, $currUrl);

        //Build a client
        $client = $this->svcFactory->createService(
            $this->getSlug(),
            $credentials,
            $this->storage,
            $this->getServiceParams()
        );

        //Step 1 - Get an access token
        if ( ! $request->query->get('code')) {
            return new RedirectResponse((string) $client->getAuthorizationUri());
        }
        else { //Step 2 - Get the access token and the information

            //Get the access token
            $client->requestAccessToken( $_GET['code'] );

            //Do a request for the data
            $rawData = $client->request($this->getInfoUrl());
            return $this->mapResponseToUserInfo($rawData);
        }
    }    
}

/* EOF: OAuthProvider.php */