<?php

namespace SilexHeadStart\Authentication;

use Symfony\Component\HttpFoundation\Request;

/**
 * User Auth Service Provider Interface
 */
interface ServiceProviderInterface
{
    /**
     * An identifier to identify the service
     */
    function getSlug();

    /**
     * Can this plugin create a new user on successful authentication?
     *
     * @return boolean
     */
    function canCreateNewUser();

    /**
     * Get some sample HTML that comprises the link to this service login
     * 
     * @param string $url  The complete URL to the service provder
     * @return string      HTML
     */
    function getLinkHtml($url);

    /**
     * Perform the login action
     *
     * Return an array of identifiers for the user if login succeeds,
     * false if login fails, or a RedirectResponse if the user should be
     * redirected to another URL as an intermediary step
     *
     * @param Symfony\Component\HttpFoundation\Request $request
     * @return array|boolean|Symfony\Component\HttpFoundation\RedirectResponse
     */
    function loginAction(Request $request);
}

/* EOF: ServiceProviderInterface.php */