<?php

namespace SilexHeadStart\Authentication;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Guzzle\Http\Client as GuzzleClient;

/**
 * Generic OAuth Service Provider for Authentication
 */
class CASProvider implements ServiceProviderInterface
{
    /**
     * @var array
     */
    private $options;

    /**
     * @var Guzzle\Http\Client
     */
    private $guzzle;

    // --------------------------------------------------------------

    /**
     * Constructor
     */
    public function __construct(GuzzleClient $guzzle, array $options)
    {
        $this->guzzle = $guzzle;

        //Set options
        $resolver = new OptionsResolver();
        $resolver->setRequired(array('url', 'slug', 'linktext', 'gateway', 'xpath_mappings', 'autocreate'));
        $resolver->setOptional(array('redirect'));
        $resolver->setAllowedTypes(array('gateway' => 'bool', 'xpath_mappings' => 'array', 'autocreate' => 'bool'));

        //Default options        
        $resolver->setDefaults(array(
            'slug'           => 'cas',
            'linktext'       => 'Login with CAS',
            'gateway'        => true,
            'redirect'       => null,
            'xpath_mappings' => array(),
            'autocreate'     => false
        ));

        //Resolve options
        $this->options = $resolver->resolve($options);
    }

    // --------------------------------------------------------------

    /**
     * An identifier to identify the service -- Maps to Oauth Factory identifiers
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->options['slug'];
    }

    // --------------------------------------------------------------

    /**
     * Can this plugin create a new user on successful authentication?
     *
     * @return boolean
     */
    public function canCreateNewUser()
    {
        return (boolean) $this->options['autocreate'];
    }

    // --------------------------------------------------------------

    /**
     * Get some sample HTML that comprises the link to this service login
     * 
     * @param string $url  The complete URL to the service provder
     * @return string      HTML
     */
    public function getLinkHtml($url)
    {
        return sprintf("<a href='%s' class='btn'>%s</a>", $url, $this->options['linktext']);
    }

    // --------------------------------------------------------------

    /**
     * Perform the login action
     *
     * @param Symfony\Component\HttpFoundation\Request $request
     * @return boolean|array|Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function loginAction(Request $request)
    {        
        //If not yet receieved ticket, we're on step 1
        if ( ! $request->query->get('ticket')) {

            $url = rtrim($this->options['url'], '/') . '/login?service=' . $this->getRedirectUrl($request);
            return new RedirectResponse($url);
        }
        
        //Else, we're on step 2
        else {
            
            $reqQuery = sprintf('service=%s&ticket=%s', $this->getRedirectUrl($request), $request->query->get('ticket'));
            $req = $this->guzzle->get(rtrim($this->options['url'], '/') . '/serviceValidate?' . $reqQuery);

            $resp = $req->send();
            $result = (string) $resp->getBody();

            return $this->parseUserInfo($result);
        }
    }

    // --------------------------------------------------------------

    /**
     * Once logged-in, get user information
     *
     * @return array|boolean  FALSE if failed, array of info if succeeded
     */
    protected function parseUserInfo($rawResult)
    {

        $xml = new \SimpleXMLElement($rawResult);
        $xml->registerXPathNamespace('cas', 'http://www.yale.edu/tp/cas');

        //Return FALSE
        if ( ! $xml->xpath('//cas:authenticationSuccess')) {
            return false;
        }

        $arr = array();
        foreach ($this->options['xpath_mappings'] as $to => $fr) {
            $val = $xml->xpath($fr);
            $arr[$to] = (isset($val[0])) ? (string) $val[0] : null;
        }

        return $arr;
    }

    // --------------------------------------------------------------

    private function getRedirectUrl(Request $request)
    {
        //Assume we redirect to the current URL
        return $this->options['redirect'] 
            ?: $request->getSchemeAndHttpHost() . $request->getBaseUrl() . $request->getPathInfo();
    }     
}

/* EOF: CASProvider.php */