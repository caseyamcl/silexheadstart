<?php

namespace SilexHeadStart\Authentication;

/**
 * Credentials Provider Interface
 */
interface CredentialsProviderInterface
{
    /**
     * Get slug of login provider name
     *
     * @return string
     */
    function getSlug();

    /**
     * Get the user based on the user's name
     *
     * @param string $userName  Identifier for the user
     * @return UserInterface|null  A UserInterface or nothing
     */
    function checkUserExists($userName);

    /**
     * Check credentials
     *
     * When combined with checkUserExists, can be used to independently
     * check the password and the username separately
     *
     * @param  string  $un
     * @param  string  $pw
     * @return boolean
     */
    function checkCredentials($un, $pw);

    /**
     * Can this plugin create a new user on successful authentication?
     *
     * @return boolean
     */
    function canCreateNewUser();    
}

/* EOF: CredentialsProviderInterface.php */