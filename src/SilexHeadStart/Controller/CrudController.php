<?php

namespace SilexHeadStart\Controller;

use Silex\ControllerCollection;
use Silex\Application;
use SilexHeadStart\Core\Controller;
use SilexHeadStart\Core\Model;
use SilexHeadStart\Core\ModelMapper;
use InvalidArgumentException, RuntimeException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\EventDispatcher\GenericEvent;
use Twig_Environment;
use ArrayObject;

/**
 * CRUD Controller
 */
abstract class CrudController extends Controller
{
    const EVENT_PRE_GET_INDEX          = 'crud.pre_get_index';
    const EVENT_PRE_GET_SINGLE         = 'crud.pre_get_single';
    const EVENT_PRE_GET_DELETE_CONFIRM = 'crud.pre_delete_confirm';
    const EVENT_PRE_SAVE_SINGLE        = 'crud.pre_save_single';
    const EVENT_PRE_DELETE_SINGLE      = 'crud.pre_delete_single';

    // --------------------------------------------------------------

    /**
     * @var FormFactory
     */
    protected $ff;

    /**
     * @var Twig_Environment
     */
    protected $twig;

    /**
     * @var SilexHeadStart\Core\ModelMapper
     */
    protected $modelMapper;

    // --------------------------------------------------------------

    /**
     * @var Request (created at runtime in self::init)
     */
    private $request;

    /**
     * @var Symfony\Component\EventDispatcher\EventDispatcherInterface (created at runtime in self::init)
     */
    private $dispatcher;

    // --------------------------------------------------------------

    public function __construct(Twig_Environment $twig, FormFactory $formFactory, ModelMapper $modelMapper)
    {
        $this->modelMapper = $modelMapper;
        $this->twig        = $twig;
        $this->ff          = $formFactory;
    }

    // --------------------------------------------------------------

    /**
     * {@inheritdoc}
     */
    protected function setRoutes(ControllerCollection $routes)
    {
        $base = rtrim($this->getBasePath(), '/');

        //Get index
        $routes->get($base, array($this, 'index'));

        //Get single item
        $routes->get($base . '/create', array($this, 'getSingle'));
        $routes->get($base . '/{id}',   array($this, 'getSingle'));

        //Post single item
        $routes->post($base,             array($this, 'postSingle'));
        $routes->post($base . '/create', array($this, 'postSingle'));
        $routes->post($base . '/{id}',   array($this, 'postSingle'));

        //Patch single item
        $routes->match($base . '/{id}', array($this, 'patchSingle'))->method('patch');

        //Delete single item
        $routes->delete($base . '/{id}', array($this, 'deleteSingle'));
    }

    // --------------------------------------------------------------

    protected function init(Application $app)
    {
        //Run parent::init...
        parent::init($app);
        
        //Need access to request
        $this->request = $app['request'];

        //Need to access dispatcher
        $this->dispatcher = $app['dispatcher'];
    }

    // --------------------------------------------------------------

    /**
     * Get the base path (something like 'admin/cards' or 'admin/stacks')
     *
     * @return string
     */
    abstract protected function getBasePath();

    // --------------------------------------------------------------

    /**
     * Get single model object
     *
     * @return SilexHeadStart\Model Object
     */
    abstract protected function getSingleModelObj($id = null);

    // --------------------------------------------------------------

    /**
     * Get index list
     *
     * @return array|Traversable  Something that can be iterated over
     */
    abstract protected function getModelObjList($offset = 0, $limit = null, $query = null);

    // --------------------------------------------------------------

    /**
     * @return Symfony\Form\AbstractType
     */
    abstract protected function getFormObj();

    // --------------------------------------------------------------

    /**
     * @return string
     */
    abstract protected function getSingleName();

    // --------------------------------------------------------------

    /**
     * @return string
     */
    abstract protected function getPluralName();

    // --------------------------------------------------------------

    public function index()
    {
        $offset = $this->queryParams('offset') ?: 0;
        $limit  = $this->queryParams('limit') ?: null;
        $query  = $this->queryParams('query') ?: null;

        //Get items
        $items = $this->getModelObjList();

        //Setup view data (as ArrayObject so it is always passed by reference)
        $viewData = new ArrayObject();

        //Fire event to modify index data before display
        $this->dispatcher->dispatch(self::EVENT_PRE_GET_INDEX, new GenericEvent($viewData, array('items' => $items)));

        //Show JSON
        if ($this->clientExpects('json')) {
            $viewData['items'] = array_map(function($v) { return $v->toArray(); }, $items->toArray());
            return $this->json($viewData);
        }

        //Or HTML
        else {

            $viewData['items'] = $items;
            return $this->twig->render($this->getIndexTemplatePath(), $viewData->getArrayCopy());
        }
    }

    // --------------------------------------------------------------

    public function getSingle($id = null)
    {
        //Delete override...
        if ($this->queryParams('delete')) {
            return $this->deleteConfirm($id);
        }

        //If 'create', id is null
        $itemObj = ($id) ? $this->getSingleModelObj($id) : $this->modelMapper->factory();

        //Get the item object or 404
        if ( ! $itemObj) {
            return $this->abort(404, "Not found");
        }

        //Setup view data (as ArrayObject so it is always passed by reference)
        $viewData = new ArrayObject();

        //Fire event to modify index data before display
        $this->dispatcher->dispatch(self::EVENT_PRE_GET_SINGLE, new GenericEvent($viewData, array('item' => $itemObj)));

        //Either render the JSON view or the create/edit page
        if ($this->clientExpects('json')) {

            $viewData['item'] = $itemObj->toArray();
            return $this->json($viewData);

        }
        else {

            $viewData['item'] = $itemObj;
            $viewData['form'] = $this->ff->create($this->getFormObj(), $itemObj)->createView();
            $viewData['submit_url'] = $this->getSiteUrl($this->resolveBasePath() . '/' . ($id ?: 'create'));

            return $this->twig->render($this->getSingleTemplatePath(), $viewData->getArrayCopy());
        }
    }

    // --------------------------------------------------------------

    public function postSingle($id = null)
    {
        //Delete override...
        if ($this->queryParams('delete')) {
            return $this->deleteSingle($id);
        }

        //If 'create', id is null
        $itemObj = ($id) ? $this->getSingleModelObj($id) : $this->modelMapper->factory();

        //If no item, 404
        if ( ! $itemObj) {
            return $this->abort(404, "Not found");
        }

        //Setup the form object
        $form = $this->ff->create($this->getFormObj(), $itemObj);
        $form->handleRequest($this->request);

        //If is valid...
        if ($form->isValid()) {

            //Fire pre-save single event
            $this->dispatcher->dispatch(self::EVENT_PRE_SAVE_SINGLE, new GenericEvent($itemObj));
            
            //Model
            $this->modelMapper->save($itemObj, true);

            //Notice
            $this->setNotice('Saved the ' . $this->getSingleName(), 'success');

            //Return
            return ($this->clientExpects('json'))
                ? $this->json(array('item' => $itemObj->toArray()), ($id) ? 200 : 201)
                : $this->redirect($this->resolveBasePath());
        }

        //If not valid, send json response or redraw the template
        else {
            $this->setNotice('Submission Error.  Check submission and try again.', 'warning');

            if ($this->clientExpects('json')) {
                return $this->json(array('errors' => $this->arrayizeErrors($form)), 400);
            }
            else {
                //Template name
                $templateName = $this->getSingleTemplatePath();

                return $this->twig->render($templateName, array(
                    'item'       => $itemObj,
                    'form'       => $form->createView(),
                    'submit_url' => ($id) ? $this->getCurrentUrl() : $this->getSiteUrl($this->resolveBasePath())
                ));                
            }
        }
    }

    // --------------------------------------------------------------

    public function deleteConfirm($id)
    {
        //If no item, 404
        if ( ! $itemObj = $this->getSingleModelObj($id)) {
            return $this->abort(404, "Not found");
        }

        //Setup view data (as ArrayObject so it is always passed by reference)
        $viewData = new ArrayObject();

        //Fire event to modify index data before display
        $this->dispatcher->dispatch(self::EVENT_PRE_GET_DELETE_CONFIRM, new GenericEvent($viewData, array('item' => $itemObj)));

        //Return JSON or HTML
        if ($this->clientExpects('json')) {

            $viewData['item'] = $itemObj->toArray();

            $this->setNotice("Send a DELETE request to this URL to delete the resource");
            return $this->json($viewData);
        }
        else {

            $form = $this->formCreateBuilder()->getForm();

            $viewData['submit_url'] = $this->getCurrentUrl() . '?delete=true';
            $viewData['form']       = $form->createView();
            $viewData['item']       = $itemObj;

            return $this->twig->render($this->getDeleteTemplatePath(), $viewData->getArrayCopy());
        }
    }

    // --------------------------------------------------------------

    public function deleteSingle($id)
    {
        //If no item, 404
        if ( ! $itemObj = $this->getSingleModelObj($id)) {
            return $this->abort(404, "Not found");
        }

        $itemId = (string) $itemObj->id;
        
        //Fire event
        $this->dispatcher->dispatch(self::EVENT_PRE_DELETE_SINGLE, new GenericEvent($itemObj));

        //Do the delete
        $this->modelMapper->delete($itemObj, true);

        return ($this->clientExpects('json'))
            ? $this->json(array('id' => $itemId))
            : $this->redirect($this->resolveBasePath());
    }

    // --------------------------------------------------------------

    /**
     * RESTful PATCH Method
     *
     * @param string $id
     */
    public function patchSingle($id)
    {
        //If no item, 404
        if ( ! $itemObj = $this->getSingleModelObj($id)) {
            return $this->abort(404, "Not found");
        }

        $params = $this->postParams();

        //Try setting the params
        try {
            foreach($params as $name => $val) {

                $callback = array($itemObj, 'set' . ucfirst($name));
                if (is_callable($callback)) {
                    call_user_func($callback, $val);
                }
                else {
                    throw new InvalidArgumentException("Cannot set property: " . $name);
                }
            }
        }
        catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage(), $e);
        }

        //Finish up (a 'finally' in PHP 5.5)
        $this->modelMapper->save($itemObj, true);

        return ($this->clientExpects('json'))
            ? $this->json(array('item' => $itemObj->toArray()))
            : $this->redirect($this->resolveBasePath());
    }

    // --------------------------------------------------------------

    /**
     * Resolve the basepath during runtime.
     *
     * Sometimes it is different than the basepath
     * defined at controller setup
     *
     * @return string
     */
    protected function resolveBasePath()
    {
        return $this->getBasePath();
    }

    // --------------------------------------------------------------

    protected function getIndexTemplatePath()
    {
        return $this->getBasePath() . '/index.html.twig';
    }   

    // --------------------------------------------------------------
    
    protected function getSingleTemplatePath()
    {
        return $this->getBasePath() . '/single.html.twig';
    } 

    // --------------------------------------------------------------

    protected function getDeleteTemplatePath()
    {
        return $this->getBasePath() . '/delete.html.twig';
    }

    // --------------------------------------------------------------

    /**
     * Arrayize Form Errors
     *
     * @link http://stackoverflow.com/questions/12554562/get-all-errors-along-with-fields-the-error-is-connected-to
     *
     * @param \Symfony\Component\Form\Form $form
     * @return array
     */
    private function arrayizeErrors(Form $form)
    {
        $errors = array();
        foreach ($form->getErrors() as $key => $error) {
            $template = $error->getMessageTemplate();
            $parameters = $error->getMessageParameters();

            foreach ($parameters as $var => $value) {
                $template = str_replace($var, $value, $template);
            }

            $errors[$key] = $template;
        }
        if ($form->count()) {
            foreach ($form as $child) {
                if (!$child->isValid()) {
                    $errors[$child->getName()] = $this->getErrorMessages($child);
                }
            }
        }
        return $errors;
    }
}

/* EOF: CrudController.php */