<?php

namespace SilexHeadStart\Controller;

use SilexHeadStart\Authentication\ServiceProviderInterface as AuthSvcProvider;
use SilexHeadStart\Core\Controller as BaseController;
use SilexHeadStart\Model\UserInterface;
use SilexHeadStart\ModelMapper\UserMapperInterface;
use SilexHeadStart\Service\UserSessionInterface;
use SilexHeadStart\Helper\AuthPluginCollection;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Silex\ControllerCollection;
use Silex\Application;
use Twig_Environment;

abstract class AuthController extends BaseController
{
    /**
     * @var Twig_Environment
     */
    private $twig;

    /**
     * @var array
     */
    private $authPlugins;

    /**
     * @var SilexHeadStart\ModelMapper\UserMapperInterface
     */
    private $userMapper;

    /**
     * @var SilexHeadStart\UserTool\UserSessionInterface
     */
    private $userSession;

    // --------------------------------------------------------------

    /**
     * @var Symfony\Component\HttpFoundation\Request
     */
    private $request;

    /**
     * @var Symfony\Component\EventDispatcher\EventDispatcherInterface (created at runtime in self::init)
     */
    private $dispatcher;

    // --------------------------------------------------------------

    public function __construct(Twig_Environment $twig, UserMapperInterface $userMapper, UserSessionInterface $userSession, AuthPluginCollection $authPlugins = null)
    {
        $this->twig        = $twig;
        $this->userMapper  = $userMapper;
        $this->userSession = $userSession;

        if ($authPlugins) {
            $this->setAuthPlugins($authPlugins);
        }
    }

    // --------------------------------------------------------------

    public function setAuthPlugins(AuthPluginCollection $authPlugins)
    {
        $this->authPlugins = $authPlugins;
    }

    // --------------------------------------------------------------

    /**
     * {@inheritdoc}
     */
    protected function setRoutes(ControllerCollection $routes)
    {
        $loginPath = rtrim($this->getLoginPath(), '/');

        $routes->get($loginPath,  array($this, 'login'))->bind('front_login');
        $routes->post($loginPath, array($this, 'doLogin'))->bind('front_login_submit');

        $routes->match($loginPath . '/{service}', array($this, 'doLogin'))->method('GET|POST')->bind('front_service_login');
        $routes->match($this->getLogoutPath(), array($this, 'logout'))->method('GET|POST')->bind('front_logout');
    }

    // --------------------------------------------------------------

    /**
     * The init method is run upon the controller executing
     *
     * Pull libraries form the DiC here in child classes
     */
    protected function init(Application $app)
    {
        //Run parent::init...
        parent::init($app);
        
        //Need access to request
        $this->request = $app['request'];

        //Need to access dispatcher
        $this->dispatcher = $app['dispatcher'];
    }

    // --------------------------------------------------------------

    /**
     * Return login path
     *
     * @return string
     */
    abstract protected function getLoginPath();

    // --------------------------------------------------------------

    /**
     * Return logout path
     *
     * @return string
     */
    abstract protected function getLogoutPath();

    // --------------------------------------------------------------

    /**
     * Return default location for which to redirect after login
     *
     * @return string
     */
    abstract protected function getLoginRedirect();

    // --------------------------------------------------------------

    /**
     * Return default location for which to redirect after logout
     */
    abstract protected function getLogoutRedirect();

    // --------------------------------------------------------------

    /**
     * Get Twig Template name for login page
     *
     * @return string
     */
    abstract protected function getLoginPageTemplate();

    // --------------------------------------------------------------

    /**
     * Display the login page or a message in JSON
     */
    public function login()
    {
        //Check if already logged-in
        if ($this->userSession->isLoggedIn()) {
            return $this->alreadyLoggedIn();
        }

        //Determine login URL
        $loginUrl = rtrim($this->getSiteUrl($this->getLoginPath()), '/');

        //Setup the data
        $data = array();
        $data['form']      = $this->getLoginForm()->createView();
        $data['submiturl'] = $loginUrl;
        
        //Add service plugins (keys are URLs, values are objects)
        $data['plugins'] = array();
        foreach ($this->authPlugins->getServicePlugins() as $plugin) {
            $data['plugins'][$loginUrl . '/' . $plugin->getSlug()] = $plugin; 
        }

        //Show the page
        return ($this->clientExpects('json'))
            ? $this->json(array('message' => 'Send POST request to the submit URL to login via API'))
            : $this->twig->render($this->getLoginPageTemplate(), $data);
    }

    // --------------------------------------------------------------

    /**
     * Perform logout action
     */
    public function logout()
    {
        if ($this->userSession->isLoggedIn()) {
            $this->userSession->removeUser();
        }

        $redirect = $this->getLogoutRedirect();
        $this->setNotice("Successfully logged out.  Goodbye.");

        return ($this->clientExpects('json'))
            ? $this->json(array('message' => "Logged out"))
            : $this->redirect($redirect);
    }    

    // --------------------------------------------------------------

    /**
     * Process login request
     */
    public function doLogin($service = null)
    {
        //Check if already logged-in
        if ($this->userSession->isLoggedIn()) {
            return $this->alreadyLoggedIn();
        }
        
        //Check to ensure login plugin exists
        if ($service) {

            return ($this->authPlugins && isset($this->authPlugins[$service]))
                ? $this->doServiceLogin($this->authPlugins[$service])
                : $this->abort(404, "Page not Found");
        }
        else {
            return $this->doCredentialsLogin();
        }

    }

    // --------------------------------------------------------------

    /**
     * Built-in do-credentials login
     *
     * @TODO: Abstact this out to its own class
     */
    protected function doCredentialsLogin()
    {
        $form = $this->getLoginForm();
        $this->formHandleRequest($form);

        if ($form->isValid()) {

            //Username
            $un = $form->get('username')->getData();
            $pw = $form->get('password')->getData();

            if ( ! $this->authPlugins OR count($this->authPlugins->getCredentialsPlugins()) == 0) {
                return $this->abort(500, 'No login methods available');
            }

            //Do credentials plugins - in-order
            foreach($this->authPlugins->getCredentialsPlugins() as $plugin) {

                //Check if the plugin succeeds
                if ($plugin->checkUserExists($un) && $plugin->checkCredentials($un, $pw)) {

                    //Does the user exist already?
                    $user = $this->userMapper->findUser($un);

                    //If no user, and we can create one, then go for it
                    if ( ! $user && $service->canCreateNewUser()) {
                        $user = $this->userMapper->factory();
                        $user->setLogin($un);
                    }

                    //If we have a user, then return processLogin and we're done
                    if ($user instanceOf UserInterface) {
                        return $this->processLogin($user);
                    }
                }
            }

        }   //end - form->isValid

        //If made it here, we did not login
        $this->setNotice('Login Failed.  Check username and password', 'warning');
        return ($this->clientExpects('json'))
            ? $this->json(array('message' => 'Login failed'))
            : $this->login();        
    }

    // --------------------------------------------------------------

    /**
     * Service-based login
     */
    protected function doServiceLogin(AuthSvcProvider $service)
    {
        //Do it!
        $result = $service->loginAction($this->request);

        //Return result of logging in
        if ($result instanceOf RedirectResponse) { //Redirect
            return $result;
        }
        elseif (is_array($result)) { //Success
            
            $serviceProc = $this->processServiceLogin($service, $result);

            if ($serviceProc instanceOf UserInterface) {
                return $this->processLogin($serviceProc);
            }
        }

        //If made it here, we did not login
        $this->setNotice('Service Login Failed', 'warning');
        return ($this->clientExpects('json'))
            ? $this->json(array('message' => 'Login failed'))
            : $this->login();
    }


    // --------------------------------------------------------------

    /**
     * Process the service login
     */
    protected function processServiceLogin(AuthSvcProvider $service, array $serviceData)
    {
        //The service must return the 'login' attribute
        if ( ! isset($serviceData['login'])) {
            return false;
        }

        //If the user login exists, just log the user in
        $user = $this->userMapper->findUser($serviceData['login']);

        if ( ! $user && $service->canCreateNewUser()) {
            $user = $this->userMapper->factory();
            $this->userMapper->setUserData($user, $serviceData);
        }

        //If we managed to find or create a user, ensure the service data is correct
        if ($user) {
            $this->userMapper->save($user, true);
        }

        //User will be null or a User object
        return $user;
    }

    // --------------------------------------------------------------

    /**
     * Process login for a user after successful credentials check
     *
     * After auth check, creates session, records login, and
     * returns appropriate response
     *
     * @param SilexHeadStart\Model\UserInterface $user
     */
    protected function processLogin(UserInterface $user)
    {
        $this->userSession->setUser($user);
        
        $user->recordLogin();
        $this->userMapper->save($user, true);

        //Set notice
        $this->setNotice(
            sprintf(
                "Welcome %s.  You are now logged-in",
                isset($user->firstName) ? $user->firstName : $user->getLogin()
            ),
            'success'
        );

        //Do appropriate action
        return ($this->clientExpects('json'))
            ? $this->json(array('message' => 'Succesful login'))
            : $this->redirect($this->getLoginRedirect());
    }

    // --------------------------------------------------------------

    /**
     * Action to take if the user is already logged in
     */
    protected function alreadyLoggedIn()
    {
        $this->setNotice("You are already logged-in");

        return ($this->clientExpects('json'))
            ? $this->json(array('message' => 'No action taken'))
            : $this->redirect($this->getLoginRedirect());
    }

    // --------------------------------------------------------------

    /**
     * Return a form type
     *
     * @param  array $data
     * @return Symfony\Component\Form\Form
     */
    protected function getLoginForm($data = array())
    {
        $builder = $this->formCreateBuilder('form', $data, 'login');

        $builder->add('username', 'text', array(
            'label'    => 'Login Name',
            'required' => true
        ));

        $builder->add('password', 'password', array(
            'label'    => 'Password',
            'required' => true
        ));

        $builder->add('submit', 'submit', array(
            'label' => 'Login'
        ));

        return $builder->getForm();
    }    
}

/* EOF: AuthController.php */