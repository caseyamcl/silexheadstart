<?php

namespace SilexHeadStart\Model;

/**
 * Interface for user objects
 */
interface UserInterface
{
    /**
     * @return mixed  Unique Identifier for the user
     */
    function getId();

    /**
     * @return array  Simple array of roles
     */
    function getRoles();

    /**
     * @param string $loginName
     */
    function setLogin($loginName);

    /**
     * @return string
     */
    function getLogin();

    /**
     * Record that the user has logged-in
     *
     * @param string $pluginName  Plugin used to login
     */
    function recordLogin($pluginName);
}

/* EOF: UserInterface.php */