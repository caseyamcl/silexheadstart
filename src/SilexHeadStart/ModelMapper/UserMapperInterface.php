<?php

namespace SilexHeadStart\ModelMapper;

use SilexHeadStart\Model\UserInterface;

/**
 * User Mapper Interface
 */
interface UserMapperInterface
{
    /**
     * Build a new User object
     *
     * @return SilexHeadStart\Model\UserInterface
     */
    function factory();

    /**
     * Find a user by the identifier property for the user
     *
     * @param string $login
     * @return SilexHeadStart\Model\UserInterface|null
     */
    function findUser($login);

    /**
     * Save a user
     *
     * @param SilexHeadStart\Model\UserInterface
     */
    function saveUser(UserInterface $user);

    /**
     * Set user data for a user
     *
     * @param UserInterface $user
     * @param array         $userData
     */
    function setUserData(UserInterface $user, array $userData);
}

/* EOF: UserMapperInterface.php */