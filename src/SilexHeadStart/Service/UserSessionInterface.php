<?php

namespace SilexHeadStart\Service;

use SilexHeadStart\Model\UserInterface;

/**
 * User Session Interface
 */ 
interface UserSessionInterface
{
    /**
     * Get the user from the session
     * 
     * @return mixed|null  Resource representing the user, or null
     */
    function getUser();

    /**
     * Set the user in the session
     *
     * @param mixed $user  Resource representing the user
     */
    function setUser(UserInterface $user);

    /**
     * Returns true if the user in the session is logged-in (identified)
     *
     * @return boolean
     */
    function isLoggedIn();

    /**
     * Is there a user in the session?
     *
     * @return boolean
     */
    function hasUser();

    /**
     * Remove the user from the session
     */
    function removeUser();
}

/* EOF: UserSessionInterface.php */