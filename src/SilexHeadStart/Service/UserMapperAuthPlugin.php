<?php

namespace SilexHeadStart\Service;

use SilexHeadStart\Authentication\CredentialsProviderInterface;
use SilexHeadStart\ModelMapper\UserMapperInterface;

class UserMapperAuthPlugin implements CredentialsProviderInterface
{
    /**
     * @var SilexHeadStart\UserMapper\UserMapperInterface
     */
    private $userMapper;

    /**
     * @var string
     */
    private $checkPwMethod;

    // --------------------------------------------------------------

    /**
     * Constructor
     *
     * @param SilexHeadStart\UserMapper\UserMapperInterface $userMapper
     * @param string                                        $checkPwMethod  Method on the User object to check passwords
     */
    public function __construct(UserMapperInterface $userMapper, $checkPwMethod = 'checkPassword')
    {
        $this->userMapper    = $userMapper;
        $this->checkPwMethod = $checkPwMethod;
    }

    // --------------------------------------------------------------

    /**
     * Get slug of login provider name
     *
     * @return string
     */
    public function getSlug()
    {
        return 'usermapper';
    }

    // --------------------------------------------------------------

    /**
     * Get the user based on the user's name
     *
     * @param string $userName  Identifier for the user
     * @return UserInterface|null  A UserInterface or nothing
     */
    public function checkUserExists($userName)
    {
        return (boolean) $this->userMapper->findUser($userName);
    }

    // --------------------------------------------------------------

    /**
     * Check a password
     *
     * @param  string  $pw
     * @return boolean
     */
    public function checkCredentials($un, $pw)
    {
        $user = $this->userMapper->findUser($un);
        return ($user && $this->checkPwMethod && call_user_func(array($user, $this->checkPwMethod), $pw));
    }

    // --------------------------------------------------------------

    /**
     * Can this plugin create a new user on successful authentication?
     *
     * @return boolean
     */
    public function canCreateNewUser()
    {
        return false;
    }      
}

/* EOF: UserMapperAuthPlugin.php */