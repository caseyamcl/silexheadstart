<?php

namespace SilexHeadStart\Helper;

use Doctrine\Common\Collections\ArrayCollection;
use InvalidArgumentException;

/**
 * Algorithm Bag
 */
class ObjectCollection extends ArrayCollection
{
    const ANY = false;

    // --------------------------------------------------------------

    /**
     * @var array
     */
    protected $_elements;

    // --------------------------------------------------------------

    public function __construct(array $elements = array())
    {
        foreach($elements as $k => $elem) {
            $this->set($k, $elem);
        }
    }
    
    // --------------------------------------------------------------

    public function set($key, $value)
    {
        $this->checkAcceptable($value);
        return parent::set($key, $value);
    }

    // --------------------------------------------------------------

    public function add($value)
    {
        $this->checkAcceptable($value);
        return parent::add($value);
    }

    // --------------------------------------------------------------

    private function checkAcceptable($value)
    {
        $acceptable = $this->acceptableClasses();

        if ($acceptable != self::ANY) {

            if ( ! is_array($acceptable)) {
                $acceptable = array($acceptable);
            }

            $acceptable = array_map(
                function($v) {return trim($v, "\\"); }, 
                $acceptable
            );

            $found = false;
            foreach($acceptable as $acceptableClassName) {

                if ( ! is_object($value)) {
                    break;
                }

                if ($value instanceOf $acceptableClassName) {
                    $found = true;
                    break;
                }
            }


            if ( ! $found) {
                throw new InvalidArgumentException(sprintf(
                    "%s must be an instance of one of the following: %s",
                    is_object($value) ? get_class($value) : gettype($value),
                    implode(', ', $acceptable)
                ));
            }
        }
    }

    // --------------------------------------------------------------

    /**
     * @return array|string|false  If false, any classes are acceptable, array or string limits
     */
    protected function acceptableClasses()
    {
        return self::ANY;
    }
}

/* EOF: ObjectCollection.php */