<?php

namespace SilexHeadStart\Helper;

use SilexHeadStart\Core\ModelMapper;
use ArrayAccess;

class MapperCollection implements ArrayAccess
{
    /**
     * @var array
     */
    private $mappers;

    // --------------------------------------------------------------

    public function __construct(array $mappers = array())
    {
        $this->mappers = array();

        foreach($mappers as $mapper) {
            $this->addMapper($mapper);
        }
    }

    // --------------------------------------------------------------

    public function __get($item)
    {
        return $this->mappers[ucfirst($item)];
    }

    // --------------------------------------------------------------

    public function __isset($item)
    {
        return (isset($this->mappers[ucfirst($item)]));
    }

    // --------------------------------------------------------------

    public function offsetExists($offset)
    {
        return ($this->__isset($this->mappers[$offset]));
    }

    public function offsetGet($offset)
    {
        return $this->mappers[ucfirst($offset)];
    }

    public function offsetSet($offset, $value)
    {
        $this->addMapper($value);
    }

    public function offsetUnset($offset)
    {
        $this->removeMapper($offset);
    }

    // --------------------------------------------------------------

    public function addMapper(ModelMapper $mapper)
    {
        $fullName  = ltrim($mapper->getModelClassName(), "\\");
        $shortName = $this->getShortName($fullName);

        $this->mappers[$fullName]  =  $mapper;
        $this->mappers[$shortName] =& $this->mappers[$fullName];
    }

    // --------------------------------------------------------------

    public function removeMapper($index)
    {
        if ( ! isset($this->mappers[$index])) {
            return;
        }

        $fullName  = $this->mappers[$index];
        $shortName = $this->getShortName($fullName);

        unset($this->mappers[$shortName]);
        unset($this->mappers[$fullName]);
    }

    // --------------------------------------------------------------

    protected function getShortName($className)
    {
        $fNExp = explode("\\", $className);
        return end($fNExp);
    }
}

/* EOF: MapperCollection.php */