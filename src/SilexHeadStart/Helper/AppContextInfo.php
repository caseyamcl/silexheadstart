<?php

namespace SilexHeadStart\Helper;

use Silex\Application as SilexApp;
use InvalidArgumentException, LogicException;
use Closure;

/**
 *  App Context Info Class
 */
class AppContextInfo
{
    const AUTO = null;
    const CLI  = 'cli';
    const WEB  = 'web';

    // --------------------------------------------------------------

    /**
     * @var string ('web' or 'cli')
     */
    protected $context;

    /**
     * @var array
     */
    protected $data;

    // --------------------------------------------------------------

    public function __construct($context = self::AUTO)
    {
        if ($context == self::AUTO) {
            $context = (php_sapi_name() == 'cli') ? self::CLI : self::WEB;
        }
        elseif ( ! in_array($context, array(self::CLI, self::WEB))) {
            throw new InvalidArgumentException(sprintf(
                "First argument for %s::__construct must be %s, or null (for auto-detect)",
                self::CLI . ', ' . self::WEB,
                get_called_class()
            ));
        }

        $this->context = $context; //set to the context name
        $this->data    = null; //set to null initially
    }

    // --------------------------------------------------------------

    public function init(SilexApp $app)
    {
        $this->data = array();

        if ($this->context == self::WEB) {
            $this->data['ipAddress'] = $app['request']->getClientIp();
            $this->data['sessionId'] = isset($app['session']) ? $app['session']->getId() : null;            
        }
        else {

            //Different OSes behave differently for this...
            $this->data['cliUser'] = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') 
                ? get_current_user() 
                : posix_getpwuid(posix_geteuid());

            //Get acommand line arguments
            global $argv;
            $this->data['cliArgs'] = $argv;
        }
    }
    
    // --------------------------------------------------------------

    /**
     * @return string
     */
    public function getContext()
    {
        return $this->context;
    }

    // --------------------------------------------------------------

    /**
     * @return array
     */
    public function getContextData()
    {
        if (is_null($this->data)) {
            throw new LogicException(sprintf("Cannot retrieve context data before executing %s::init", get_called_class()));
        }

        return $this->data;
    }
}

/* EOF: AppContextInfo.php */