<?php
 
namespace SilexHeadStart\Helper;
 
use Doctrine\Common\Persistence\AbstractManagerRegistry;
use Silex\Application;
use BadMethodCallException;

/**
 * References Doctrine connections and entity/document managers
 * so we can use the Doctrine Mongo Form Extension
 *
 * @author Саша Стаменковић <umpirsky@gmail.com>
 * @link https://gist.github.com/umpirsky/3939837
 */
class DoctrineManagerRegistry extends AbstractManagerRegistry
{
    /**
     * @var Silex\Application
     */
    protected $container;

    // --------------------------------------------------------------
 
    protected function getService($name)
    {
        return $this->container[$name];
    }

    // --------------------------------------------------------------
 
    protected function resetService($name)
    {
        unset($this->container[$name]);
    }

    // --------------------------------------------------------------
 
    public function getAliasNamespace($alias)
    {
        throw new BadMethodCallException('Namespace aliases not supported.');
    }
 
    // --------------------------------------------------------------

    public function setContainer(Application $container)
    {
        $this->container = $container;
    }
}

/* EOF DoctrineManagerRegistry.php */