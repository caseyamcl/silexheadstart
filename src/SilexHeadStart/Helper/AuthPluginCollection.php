<?php

namespace SilexHeadStart\Helper;

use SilexHeadStart\Authentication\ServiceProviderInterface;
use SilexHeadStart\Authentication\CredentialsProviderInterface;
use ArrayAccess, IteratorAggregate, ArrayIterator;
use InvalidArgumentException;


class AuthPluginCollection implements ArrayAccess, IteratorAggregate
{
    /**
     * @var array
     */
    private $servicePlugins;

    /**
     * @var array
     */
    private $credentialsPlugins;

    /**
     * @var array
     */
    private $plugins;

    // --------------------------------------------------------------

    public function __construct(array $plugins = array())
    {
        $this->servicePlugins     = array();
        $this->credentialsPlugins = array();

        foreach($plugins as $plugins) {
            $this->addPlugin($plugins);
        }
    }

    // --------------------------------------------------------------

    public function __get($item)
    {
        return $this->plugins[$item];
    }

    // --------------------------------------------------------------

    public function __isset($item)
    {
        return (isset($this->plugins[$item]));
    }

    // --------------------------------------------------------------

    public function offsetExists($offset)
    {
        return $this->__isset($offset);
    }

    public function offsetGet($offset)
    {
        return $this->plugins[$offset];
    }

    public function offsetSet($offset, $value)
    {
        $this->addPlugin($value);
    }

    public function offsetUnset($offset)
    {
        $this->removePlugin($offset);
    }

    // --------------------------------------------------------------

    public function getServicePlugins()
    {
        return $this->servicePlugins;
    }

    // --------------------------------------------------------------

    public function getCredentialsPlugins()
    {
        return $this->credentialsPlugins;
    }

    // --------------------------------------------------------------

    public function getIterator()
    {
        return new ArrayIterator($this->plugins);
    }

    // --------------------------------------------------------------

    public function addPlugin($plugin)
    {   
        if ($plugin instanceOf CredentialsProviderInterface) {
            $type = 'credentialsPlugins';
        }
        elseif ($plugin instanceOf ServiceProviderInterface) {
            $type = 'servicePlugins';
        }
        else {
            throw new InvalidArgumentException(sprintf(
                "%s::addPlugin requires either CredentialsProviderInterface or ServiceProviderInterface instance",
                get_called_class()
            ));
        }

        //Set the values
        $typeArr =& $this->$type;        
        $typeArr[$plugin->getSlug()] = $plugin;
        $this->plugins[$plugin->getSlug()] =& $typeArr[$plugin->getSlug()];
    }

    // --------------------------------------------------------------

    public function removePlugin($index)
    {
        if ( ! isset($this->plugins[$index])) {
            return;
        }

        unset($this->plugins[$index]);
        if (isset($this->servicePlugins[$index])) {
            unset($this->servicePlugins[$index]);
        }
        else {
            unset($this->credentialsPlugins[$index]);   
        }
    }
}

/* EOF: AuthPluginCollection.php */