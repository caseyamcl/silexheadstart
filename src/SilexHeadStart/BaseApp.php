<?php

namespace SilexHeadStart;

use Silex\Application as SilexApp;
use Silex\Provider as SilexBuiltinProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Console\Application as ConsoleApp;
use Symfony\Component\Yaml\Yaml;
use Whoops\Provider\Silex\WhoopsServiceProvider;
use Whoops\Handler\JsonResponseHandler;
use SilexHeadStart\Core\Command as ConsoleCommand;
use RuntimeException, Exception;
use Configula, Pimple;
use ReflectionClass;

/**
 * Greyhound App
 */
abstract class BaseApp
{
    const PRODUCTION  = 1;
    const DEVELOPMENT = 2;

    const AUTO = null;

    // --------------------------------------------------------------

    /**
     * @var int
     */
    protected $mode;

    /**
     * @var Symfony\Components\EventDispatcher\EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @var Silex\Application
     */
    private $silexApp;

    /**
     * @var Symfony\Component\Console\Application
     */
    private $cliApp;

    /**
     * @var string -- Not to be confused with self::basePath() method
     */
    private $basePath;

    // --------------------------------------------------------------

    /**
     * Static Entry Point
     */
    public static function main($mode = self::PRODUCTION, $basePath = self::AUTO)
    {
        $cls = get_called_class();
        $that = new $cls($mode, $basePath);
        return $that->exec();
    }

    // --------------------------------------------------------------

    /**
     * Constructor
     *
     * @param int $mode  self::DEVELOPMENT or self::PRODUCTION
     */
    public function __construct($mode = self::PRODUCTION, $basePath = self::AUTO)
    {
        //Set the mode
        $this->mode = $mode;
        $this->basePath = $basePath ?: realpath(dirname(realpath($_SERVER['SCRIPT_FILENAME'])));

        //Init Pimple
        $this->silexApp = new SilexApp(array(
            'slug' => $this->getSlug()
        ));
    }

    // --------------------------------------------------------------

    /**
     * @return string  A 'slug' (machine-name) for the app
     */ 
    abstract public function getSlug();

    // --------------------------------------------------------------

    /**
     * Execute the application
     *
     * Executes the CLI or web app depending on the output of php_sapi_name()
     */
    public function exec()
    {
        //Set base and common libraries
        $this->loadBaseLibraries($this->silexApp);
        $this->loadCommonLibraries($this->silexApp);

        //Execute the application
        return (php_sapi_name() == 'cli')
            ? $this->execCli()
            : $this->execWeb();
    }

    // --------------------------------------------------------------

    /**
     * Execute the program
     */
    protected function execWeb(Request $request = null)
    {
        $this->loadBaseWebLibraries($this->silexApp);
        $this->loadWebLibraries($this->silexApp);
        
        //Pointer
        $silexApp =& $this->silexApp;

        //Mounter
        $cntlrMounter = function(Core\Controller $cntlr) use ($silexApp) {
            $silexApp->mount('', $cntlr);
        };

        //Mount the controllers
        foreach ($this->mountControllers($this->silexApp) as $controller) {
            $cntlrMounter($controller);
        }

        //Run it
        return $this->silexApp->run($request);
    }

    // --------------------------------------------------------------

    /**
     * Execute CLI App
     */
    protected function execCli(InputInterface $input = null, OutputInterface $output = null)
    {
        $this->loadCliLibraries($this->silexApp);

        //Pointers
        $cliApp = new ConsoleApp($this->getSlug());
        $silexApp =& $this->silexApp;

        //Event dispatcher
        $cliApp->setDispatcher($silexApp['dispatcher']);

        //Mounter
        $cmdMounter = function(ConsoleCommand $command) use ($cliApp, $silexApp) {
            $command->init($silexApp);
            $cliApp->add($command);
        };

        //Mount the commands
        foreach ($this->mountCommands($silexApp) as $command) {
            $cmdMounter($command);
        }

        //Run it
        $cliApp->run($input, $output);
    }

    // --------------------------------------------------------------

    /**
     * Get a path relative to the basepath
     *
     * @param  string  $subpath
     * @return string  Full path
     */
    public function basepath($subpath = null)
    {
        return $this->basePath . '/' . trim($subpath, '/');
    }

    // --------------------------------------------------------------

    /**
     * Return the path to the main application
     *
     * @param  string $subPath
     * @return string Full path
     */
    public function appPath($subpath = null)
    {
        $refl = new ReflectionClass(get_called_class());
        $dir  = dirname($refl->getFileName());
        return $dir . '/' . trim($subpath, '/');
    }

    // --------------------------------------------------------------

    /**
     * @return array  Array of controllers
     */
    protected function mountControllers(SilexApp $app)
    {
        //meant to be overridden
        return array();
    }

    // --------------------------------------------------------------

    /**
     * @return array  Array of commands
     */
    protected function mountCommands(SilexApp $app)
    {
        //Load Commands
        return array();
    }

    // --------------------------------------------------------------

    /**
     * Load web-specific libraries
     *
     * @param Silex\Application $app
     */
    protected function loadWebLibraries(SilexApp $app)
    {
        //Load them libraries
    }

    // --------------------------------------------------------------

    /**
     * Load cli-specific libraries
     *
     * @param Silex\Application $app
     */
    protected function loadCliLibraries(SilexApp $app)
    {
        //meant to be overridden
    }

    // --------------------------------------------------------------

    /**
     * Load common libraries to both web and CLI
     *
     * @param Silex\Application $app
     */
    protected function loadCommonLibraries(SilexApp $app)
    {
        //meant to be overridden
    }

    // --------------------------------------------------------------

    /**
     * Load base web libraries
     *
     * @param Silex\Application $app
     */    
    private function loadBaseWebLibraries(SilexApp $app)
    {
        //$app['app.context']
        $app->register(new Provider\AppContextInfoProvider());

        //$app['session']
        $app->register(
            new SilexBuiltinProvider\SessionServiceProvider(), array('session.storage.options' => array(
                'name'            => $this->getSlug(), 
                'cookie_lifetime' => $app['config']->session_expire ?: 7200
            )
        ));

        //$app['form']
        $app->register(new SilexBuiltinProvider\FormServiceProvider());

        //$app['resolver']
        $app->register(new SilexBuiltinProvider\ServiceControllerServiceProvider());

        //$app['whoops']
        if ($app['debug']) {
            $app->register(new WhoopsServiceProvider());

            $app['whoops'] = $app->extend('whoops', function($whoops) {
                $whoops->pushHandler(new JsonResponseHandler());
                return $whoops;
            });
        }

        //$app['notices']
        $app['notices'] = new Service\Notices($app['session']);        

    }

    // --------------------------------------------------------------

    private function loadBaseLibraries()
    {
        //Recursive pointer
        $app =& $this->silexApp;

        //Development mode?
        if ($this->mode == self::DEVELOPMENT) {
            $app['debug'] = true;
        }         

        //$app['config']
        $app['config'] = (is_readable($this->basePath('config')))
            ? new Configula\Config($this->basePath('config'), $this->loadConfigDefaults())
            : new Configula\Config(null, $this->loadConfigDefaults());

        //$app['monolog']
        $app->register(new Provider\MonologServiceProvider(), array(
            'monolog.logfile' => $app['config']->logfile ?: sys_get_temp_dir() . '/' . $this->getSlug() . '.log'
        ));

        //$app['mongo'] - DocumentManager
        $app->register(new Provider\DoctrineMongoServiceProvider(), array(
            'mongo.documents_path' => $this->appPath('Model')
        ));

        //$this['validation']
        $app->register(new SilexBuiltinProvider\ValidatorServiceProvider());

        //Translation Service Provider (required by forms provider)
        $app->register(new SilexBuiltinProvider\TranslationServiceProvider(), array(
            'locale_fallback' => 'en',
        ));
    }

    // --------------------------------------------------------------

    protected function loadConfigDefaults()
    {
        return array();
    }    
}

/* EOF: App.php */