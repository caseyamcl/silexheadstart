<?php

namespace SilexHeadStart\Core;

use Doctrine\Common\Persistence\ObjectManager;
use LogicException, ReflectionClass;
use Doctrine\ODM\MongoDB\Events;

/**
 * A class for adding abstraction and control to individual models
 */
abstract class ModelMapper
{
    /**
     * @var Doctrine\Common\Persistence\ObjectManager
     */
    protected $om;

    // --------------------------------------------------------------
 
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;

        if ($this->hasEvents()) {
            $this->registerEvents();
        }
    }

    // --------------------------------------------------------------
 
    /**
     * @return string  Fully-qualified classname for the model
     */
    abstract public function getModelClassName();

    /**
     * @return boolean  Whether or not to enforce indexes by default
     */
    abstract protected function enforceIndexes();

    // --------------------------------------------------------------

    public function factory()
    {
        $class = new ReflectionClass($this->getModelClassName());
        return $class->newInstanceArgs(func_get_args());
    }

    // --------------------------------------------------------------

    /**
     * Finds an object by its primary key / identifier.
     *
     * @param int $id The identifier.
     * @return object The object.
     */
    public function find($id)
    {
        return $this->getRepository()->find($id);
    }

    // --------------------------------------------------------------

    public function findAll()
    {
        return $this->getRepository()->findAll();
    }

    // --------------------------------------------------------------

    /**
     * Finds objects by a set of criteria.
     *
     * Optionally sorting and limiting details can be passed. An implementation may throw
     * an UnexpectedValueException if certain values of the sorting or limiting details are
     * not supported.
     *
     * @throws \UnexpectedValueException
     * @param array $criteria
     * @param array|null $orderBy
     * @param int|null $limit
     * @param int|null $offset
     * @return mixed The objects.
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->getRepository()->findBy($criteria, $orderBy, $limit, $offset);
    }

    // --------------------------------------------------------------

    /**
     * Finds a single object by a set of criteria.
     *
     * @param array $criteria
     * @return object The object.
     */
    public function findOneBy(array $criteria)
    {
        return $this->getRepository()->findOneBy($criteria);
    }

    // --------------------------------------------------------------

    /**
     * @return Doctrine\Common\Persistence\ObjectRepository
     */
    public function getRepository()
    {
        return $this->om->getRepository($this->getModelClassName());
    }

    // --------------------------------------------------------------

    public function save(Model $item, $flush = false, array $flushOptions = array())
    {
        $this->om->persist($item);

        if ($flush) {
            $this->flush($flushOptions);
        }
    }

    // --------------------------------------------------------------

    public function delete(Model $item, $flush = false)
    {
        $this->om->remove($item);

        if ($flush) {
            $this->flush();
        }
    }

    // --------------------------------------------------------------

    public function flush(array $flushOptions = array())
    {
        $flushOptions = array_merge(
            array('safe' => (boolean) $this->enforceIndexes()),
            $flushOptions
        );
        
        $this->om->flush();
    }

    // --------------------------------------------------------------

    public function getObjectManager()
    {
        return $this->om;
    }

    // --------------------------------------------------------------

    public function createQueryBuilder()
    {
        $repo = $this->getRepository();

        if (is_callable(array($repo, 'createQueryBuilder'))) {
            return $repo->createQueryBuilder();
        }
        else {
            throw new LogicException("createQueryBuilder is not a feature of your persistence library");
        }
    }

    // --------------------------------------------------------------

    /**
     * Indicate any events that this model mapper uses
     *
     * @return array
     */
    protected function hasEvents()
    {
        return array();
    }

    // --------------------------------------------------------------

    /**
     * Register all events
     */
    private function registerEvents()
    {
        //Sanity check
        if ( ! is_callable(array($this->om, 'getEventManager'))) {
            throw new LogicException(sprintf("
                The Doctrine persistence manager (%s) does not support the getEventManager() api call",
                get_class($this->om)
            ));
        }

        //Get the event manager
        $eMgr = $this->om->getEventManager();

        foreach($this->hasEvents() as $eventName) {
            $eMgr->addEventListener($eventName, $this);
        }
    }
}

/* EOF: ModelMapper.php */