<?php

namespace SilexHeadStart\Core;

use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\AbstractType;
use Exception;

/**
 * Base controller
 */
abstract class Controller implements ControllerProviderInterface
{
    /**
     * @var Silex\Application
     */
    private $app;

    /**
     * @var boolean  Whether or not debug mode is enabled
     */
    private $debugMode;

    // --------------------------------------------------------------

    /**
     * Connect method
     *
     * @param Silex\Application $app
     */
    public function connect(Application $app)
    {
        //Register a custom error handler
        //Use 400 priority to ensure this loads after the before() middleware
        //in the main app file, but before the other error handlers (like FilpWhoops)
        $app->error(array($this, 'handleError'), 400);
        $this->debugMode = $app['debug'];

        //Setup the app as a controller variable
        $this->app = $app;

        //Get controllers factory
        $routes = $app['controllers_factory'];

        //Call setRoutes from the child class to set the routes
        $this->setRoutes($routes);

        //Add init as before hook
        $app->before(array($this, 'initialize'));

        //Register the routes
        return $routes;
    }

    // --------------------------------------------------------------

    /**
     * Init method is run as before() hook and runs the protected
     * init() method
     */
    final public function initialize()
    {
        $ob = $this->app['request']->attributes->get('_controller');

        if ($this instanceOf $ob[0]) {
            $this->preInit($this->app);
            return $this->init($this->app);    
        }        
    }

    // --------------------------------------------------------------

    /**
     * Handle an error gracefully if possible or desirable
     *
     * @param Exception $e
     * @param int       $code
     */
    public function handleError(Exception $e, $code)
    {
        //For JSON, we always return the same response
        if ($this->clientExpects('json')) {
            return $this->json(array('message' => $e->getMessage()), $code);
        }

        //Intercept 401 requests to the login page (non-JSON), and a login path is defined
        elseif ($code == 401 && $this->loginPath()) {
            $this->setNotice($e->getMessage() ?: 'This action requires login', 'warning');
            return $this->redirect($this->loginPath());
        }

        //Try to show pretty errors if not in debug mode, and always
        //for 404 errors
        elseif (isset($this->twig) && ( ! $this->debugMode OR $code == 404)) {

            $specificErrorPage = 'errors/' . $code . '.html.twig';
            $generalErrorPage  = 'errors/' . $code . '.html.twig';
            $viewData = array('exception' => $e, 'code' => $code);

            if ($this->twig->getLoader()->exists($specificErrorPage)) {
                return $this->twig->render($specificErrorPage, $viewData);
            }
            elseif ($this->twig->getLoader()->exists($generalErrorPage)) {
                return $this->twig->render($generalErrorPage, $viewData);   
            }
        }

        //If made it here, perform default action 
        return;
    }

    // --------------------------------------------------------------

    /**
     * Set the routes
     *
     * Be sure to only set routes in here, and load all other resources
     * in self::init() for performance reasons
     *
     * Run $app->get(), $app->match(), etc.. in this method
     *
     * @param Silex\Application $app
     */
    abstract protected function setRoutes(ControllerCollection $routes);  

    // --------------------------------------------------------------

    /**
     * Optional pre-init method that runs before init()
     *
     * Useful for abstracts that extend this class, but are
     * themselves meant to be extended
     */
    protected function preInit(Application $app)
    {
        //Do nothing; this is meant to be overridden
    }

    // --------------------------------------------------------------

    /**
     * The init method is run upon the controller executing
     *
     * Pull libraries form the DiC here in child classes
     */
    protected function init(Application $app)
    {        
        //Do nothing; this is meant to be overridden
    }

    // --------------------------------------------------------------   

    /**
     * Log a message to Monolog
     *
     * @param string $level
     * @param string $message
     * @param array  $context
     * @param boolean  Whether the record has been processed
     */
    protected function log($level, $message, array $context = array())
    {
        $method = 'add' . ucfirst(strtolower($level));
        if (method_exists($this->app['monolog'], $method)) {
            return call_user_func(array($this->app['monolog'], $method), $message, $context);
        }
        else {
            throw new \InvalidArgumentException(sprintf(
                "The logging level '%s' is invalid. See Monolog documentation for valid log levels",
                $level
            ));
        }
    }    

    // --------------------------------------------------------------   

    /**
     * Shortcut for debugging
     *
     * @param mixed $item
     * @param string $msg  Optional, if $item is not a string
     */
    protected function debug($item, $msg = null)
    {
        if (is_string($item)) {
            return $this->log('debug', $item);
        }
        else {
            return $this->log('debug', $msg ?: 'Debug', (array) $item);
        }
    }

    // --------------------------------------------------------------   

    /**
     * Get query string parameters from input
     *
     * @param string|null $which
     * @return array|mixed|null
     */
    protected function queryParams($which = null)
    {
        return ( ! is_null($which))
            ? $this->app['request']->query->get($which)
            : $this->app['request']->query->all();
    }

    // --------------------------------------------------------------

    /**
     * Get post parameters from input
     *
     * @param string|null $which
     * @return array|mixed|null
     */
    protected function postParams($which = null)
    {
        return ( ! is_null($which))
            ? $this->app['request']->request->get($which)
            : $this->app['request']->request->all();
    }

    // --------------------------------------------------------------

    /**
     * Create a form builder
     *
     * @param string|Symfony\Component\Form\FormBuidlerInterface  $type
     * @param array|object                                        $data
     * @param string                                              $name
     * @return Symfony\Component\Form\FormBuilderInterface
     */
    protected function formCreateBuilder($type = 'form', $data = array(), $name = null)
    {
        $name = $name ?: 'form';
        return $this->app['form.factory']->createNamedBuilder($name, $type, $data);
    }


    // --------------------------------------------------------------

    protected function formCreate(AbstractType $formType, $data = array())
    {
        return $this->app['form.factory']->create($formType, $data);
    }

    // --------------------------------------------------------------

    /**
     * Handle a form request
     *
     * @param Symfony\Component\Form\Form
     */
    protected function formHandleRequest(Form $form)
    {
        $form->handleRequest($this->app['request']);
    }

    // --------------------------------------------------------------

    protected function formErrorsToNotices(array $formErrors)
    {
        foreach($formErrors as $err) {
            $this->setNotice($err->getMessage(), 'bad');
        }
    } 

    // --------------------------------------------------------------

    /**
     * Abort Shortcut
     *
     * @param int $code
     * @param string $message
     */
    protected function abort($code, $message)
    {
        return $this->app->abort($code, $message);
    }

    // --------------------------------------------------------------

    /**
     * Redirect to another path in the app
     *
     * @param string $path
     * @return  Redirection (halts app and redirects)
     */
    protected function redirect($path)
    {
        //Ensure left slash
        $path = '/' . ltrim($path, '/');

        //Do it
        return $this->app->redirect($this->getSiteUrl() . $path);
    }

    // --------------------------------------------------------------

    protected function getCurrentUrl()
    {
        return $this->getSiteUrl($this->app['request']->getPathInfo());
    }

    // --------------------------------------------------------------

    protected function getSiteUrl($subPath = '')
    {
        $siteUrl = $this->app['request']->getSchemeAndHttpHost() . $this->app['request']->getBaseUrl();

        return ( ! empty($subPath)) ? $siteUrl . '/' . ltrim($subPath, '/') : $siteUrl;
    }

    // --------------------------------------------------------------

    /**
     * Shortcut function for sendFile
     */
    protected function sendFile($file, $status = 200, $headers = array(), $contentDisposition = null)
    {
        return $this->app->sendfile($file, $status, $headers, $contentDisposition);
    }

    // --------------------------------------------------------------

    /**
     * Shortcut function for stream
     */
    protected function stream($callback, $status = 200, $headers = array())
    {
        return $this->app->stream($callback, $status, $headers);
    }


    // --------------------------------------------------------------

    /**
     * Shortcut function for JSON
     *
     * @return string
     */
    protected function json($output, $code = 200, $headers = array())
    {
        return $this->app->json($output, $code, $headers);
    }

    // --------------------------------------------------------------

    /**
     * Is the request from XmlHttp?
     *
     * @return boolean
     */
    protected function isAjax()
    {
        return $this->app['request']->isXmlHttpRequest();
    }

    // --------------------------------------------------------------

    /**
     * Get the request method
     *
     * @return string
     */
    protected function getRequestMethod()
    {
        return $this->app['request']->getMethod();    
    }


    // --------------------------------------------------------------

    /**
     * Get raw request content
     *
     * @return string
     */
    protected function getRequestContent()
    {
        return $this->app['request']->getContent();
    }
        
    // --------------------------------------------------------------

    /**
     * Set a notice
     *
     * @param string $notice
     * @param string $level
     * @param string $scope
     */
    protected function setNotice($message, $type = 'info', $scope = 'global')
    {
        $this->app['notices']->add($message, $type, $scope);
    }  
    
    
    // --------------------------------------------------------------

    /**
     * Check if the client expects a certain content-type
     *
     * e.g.  if ($this->clientExpects('application/json'))
     *       ...
     *
     * Can use shorthand: 'html', 'json', 'xml'
     * 
     * @param  string|array $mimeType
     * @param  boolean      $strict  If false, then *\/* will always return true
     * @return boolean
     */
    protected function clientExpects($mimeType, $strict = true)
    {
        //Shorthand mappings
        $mappings = array(
            'json'  => array('application/json'),
            'html'  => array('text/html', 'application/xhtml+xml'),
            'xml'   => array('application/xml')
        );

        //Using shorthand query parameter override?
        $override = $this->queryParams('response_format');
        if ($override && in_array($override, array_keys($mappings))) {
            return true;
        }
        else {
            
            //Using shorthand?
            $expected = (isset($mappings[$mimeType])) 
                ? $mappings[$mimeType] : (array) $mimeType;

            //Client Accept Headers
            $accepted = $this->app['request']->getAcceptableContentTypes();

            //TODO: I SHOULD BE ABLE TO REMOVE THIS.  WTF IS GOING ON BELOW??
            return count(array_intersect($expected, $accepted)) > 0;

            //Return results
            return ($strict)
                ? count(array_intersect($expected, $accepted)) > 0
                : count(array_intersect($expected, $accepted)) > 0 OR in_array('*/*', $accepted);
        }
    }

    // --------------------------------------------------------------

    /**
     * Optionally return a login path to redirect to in case of 401 errors
     *
     * @return string|boolean  Return FALSE to disable this functionality (default)
     */
    protected function loginPath()
    {
        return false;
    }
}

/* EOF: Controller.php */