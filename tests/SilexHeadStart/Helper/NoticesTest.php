<?php

namespace SilexHeadStart\Helper;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;
use PHPUnit_Framework_TestCase;

/**
 * Notices Test
 */
class NoticesTests extends PHPUnit_Framework_TestCase
{
    public function testInstantiateSucceeds()
    {
        $obj = new Notices($this->getSessionObj());
        $this->assertInstanceOf('SilexHeadStart\Helper\Notices', $obj);
    }

    // --------------------------------------------------------------

    public function testAddDataCanBeRetrieved()
    {
        $obj = new Notices($this->getSessionObj());
        $obj->add("Hi", 'test', 'testScope');
        $result = $obj->get('test', 'testScope');
        $this->assertEquals('Hi', $result[0]->message);
    }

    // --------------------------------------------------------------
    
    protected function getSessionObj()
    { 
        return new Session(new MockArraySessionStorage());
    }
}

/* EOF: NoticesTest.php */