<?php

namespace SilexHeadStart;

use Symfony\Component\HttpFoundation\Request;
use PHPUnit_Framework_TestCase, Mockery;

class BaseAppTest extends PHPUnit_Framework_TestCase
{
    public function testInstantiateSucceeds()
    {
        $this->assertInstanceOf('\SilexHeadStart\BaseApp', $this->getObj());
    }

    // --------------------------------------------------------------

    protected function buildRequest($path = '/test')
    {
        $request = Request::create('http://ex.local' . $path);
        return $request;
    }

    // --------------------------------------------------------------

    protected function getObj()
    {
        $mockApp = Mockery::mock('\SilexHeadStart\BaseApp');
        return $mockApp;
    }
}

/* EOF: BaseAppTest.php */