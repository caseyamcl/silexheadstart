<?php

//Autoload file
$autoloader = __DIR__ . '/../vendor/autoload.php';

if ( ! is_readable($autoloader)) {
    die("Not yet setup.  Run composer to install dependencies");
}

//Custom autoloader thing
require_once(__DIR__ . '/../vendor/composer/ClassLoader.php');
$anotherAL = new \Composer\Autoload\ClassLoader();
$anotherAL->set('ExampleApp', __DIR__ . '/src');
$anotherAL->register();


ExampleApp\App::main();

/* EOF: index.php */